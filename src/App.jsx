import { Route, Routes, Link } from "react-router-dom";
import "./App.css";
import Home from "./components/Home";
import About from "./components/About";
import Contact from "./components/contact";

function App() {
  return (
    <>
      <nav>
        <div className="logo">
          <p className="Heading">
            <Link className="link" to="/">
              Kalvium
            </Link>
          </p>
        </div>
        <div className="options">
          <ul>
            <li>
              <Link className="link" to="/About">
                About
              </Link>
            </li>
            <li>
              <Link className="link" to="/Contact">
                Contact
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/About" element={<About />} />
        <Route path="/Contact" element={<Contact />} />
      </Routes>
    </>
  );
}

export default App;
